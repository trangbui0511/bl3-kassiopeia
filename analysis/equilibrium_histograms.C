void equilibrium_histograms ()
{
  TString file1("output/Kassiopeia/ProtonTrapSimulation.root");
  TString file2("output/Kassiopeia/ProtonTrapEquilibrium.root");
  TString tree("component_track_world_DATA");

  TFile f1(file1);
  TTree *T1 = (TTree*) f1.Get(tree);

  std::vector<TString> list;
  list.push_back("final_kinetic_energy");
  list.push_back("final_position_x");
  list.push_back("final_position_y");
  list.push_back("final_position_z");
  list.push_back("final_momentum_x");
  list.push_back("final_momentum_y");
  list.push_back("final_momentum_z");

  TFile f2(file2,"RECREATE");
  f2.cd();
  for (auto i = list.begin(); i != list.end(); i++) {
    T1->Draw(*i + ">>" + *i,"","GOFF");
  }
  f2.Write();
  f2.ls();
}
