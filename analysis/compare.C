void compare ()
{
  TString file1("output/Kassiopeia/ProtonTrapSimulation_exact_rkdp853_1e12_075_8.root");
  TString file2("output/Kassiopeia/ProtonTrapSimulation_fast_rk54_1e6_075_5.root");
  TString tree("component_track_world_DATA");

  TFile f1(file1);
  TTree *T1 = (TTree*) f1.Get(tree);
  TFile f2(file2);
  TTree *T2 = (TTree*) f2.Get(tree);

  T1->AddFriend(T2, "T2");

  T1->SetAlias("final_position_xp","-sin(18*3.14159265/180.)*(final_position_z-1.3)+cos(18*3.14159265/180.)*(final_position_x-0.111)");
  T1->SetAlias("final_position_yp","final_position_y");
  T1->SetAlias("final_position_zp","cos(18*3.14159265/180.)*(final_position_z-1.3)+sin(18*3.14159265/180.)*(final_position_x-0.111)");

  T1->SetAlias("T2.final_position_xp","-sin(18*3.14159265/180.)*(T2.final_position_z-1.3)+cos(18*3.14159265/180.)*(T2.final_position_x-0.111)");
  T1->SetAlias("T2.final_position_yp","T2.final_position_y");
  T1->SetAlias("T2.final_position_zp","cos(18*3.14159265/180.)*(T2.final_position_z-1.3)+sin(18*3.14159265/180.)*(T2.final_position_x-0.111)");

  T1->SetAlias("diff_final_position_xp","final_position_xp-T2.final_position_xp");
  T1->SetAlias("diff_final_position_yp","final_position_yp-T2.final_position_yp");
  T1->SetAlias("diff_final_position_zp","final_position_zp-T2.final_position_zp");

  TString leaf("diff_final_position_yp:diff_final_position_xp");
  T1->SetMarkerStyle(20);
  T1->SetMarkerColor(kBlack);
  T1->Draw("final_position_yp:final_position_xp","terminator_name==\"term_proton_target\"");
  T1->SetMarkerColor(kRed);
  T1->Draw(leaf,"terminator_name==\"term_proton_target\"","same");
}
